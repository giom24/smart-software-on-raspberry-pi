package main

func main() {
}

func Fib(n int) int {
	if n < 2 {
		return n
	}
	return Fib(n-1) + Fib(n-2)
}

func Concat(n int) {
	var result string

	for i := 0; i < n; i++ {
		result = result + "a"
	}
}
