\babel@toc {ngerman}{}
\beamer@sectionintoc {1}{Einleitung}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Smarte Projekte}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Raspberry Pi}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Smarte Projekte auf dem Raspberry Pi}{6}{0}{1}
\beamer@sectionintoc {2}{Unikernel}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Was sind Unikernel}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Ein Adress Maschinen}{9}{0}{2}
\beamer@subsectionintoc {2}{3}{Arten von Unikernelen}{11}{0}{2}
\beamer@subsectionintoc {2}{4}{Bibliotheks Betriebssystem}{12}{0}{2}
\beamer@subsectionintoc {2}{5}{Vor- und Nachteile}{15}{0}{2}
\beamer@subsubsectionintoc {2}{5}{1}{Vorteile}{15}{0}{2}
\beamer@subsubsectionintoc {2}{5}{2}{Nachteile}{16}{0}{2}
\beamer@subsectionintoc {2}{6}{Technologien im Vergleich}{17}{0}{2}
\beamer@subsubsectionintoc {2}{6}{1}{Virtuelle Maschinen}{17}{0}{2}
\beamer@subsubsectionintoc {2}{6}{2}{Linux Container}{19}{0}{2}
\beamer@subsubsectionintoc {2}{6}{3}{Unikernel}{21}{0}{2}
\beamer@sectionintoc {3}{GoKrazy}{24}{0}{3}
\beamer@subsectionintoc {3}{1}{Was ist GoKrazy}{24}{0}{3}
\beamer@subsectionintoc {3}{2}{Wie funktioniert GoKrazy}{25}{0}{3}
\beamer@subsubsectionintoc {3}{2}{1}{Toolchain}{25}{0}{3}
\beamer@subsubsectionintoc {3}{2}{2}{Dateisysteme}{26}{0}{3}
\beamer@subsubsectionintoc {3}{2}{3}{Boot}{28}{0}{3}
\beamer@subsubsectionintoc {3}{2}{4}{Initialisierungs System}{29}{0}{3}
\beamer@subsubsectionintoc {3}{2}{5}{Userland}{30}{0}{3}
\beamer@subsubsectionintoc {3}{2}{6}{Webinterface}{31}{0}{3}
\beamer@subsubsectionintoc {3}{2}{7}{Aktualisierungen \IeC {\"u}ber das Netzwerk}{32}{0}{3}
\beamer@subsubsectionintoc {3}{2}{8}{Kernel Konfiguration}{33}{0}{3}
\beamer@subsubsectionintoc {3}{2}{9}{Debugging}{34}{0}{3}
\beamer@sectionintoc {4}{Raspbian vs. GoKrazy}{35}{0}{4}
\beamer@subsectionintoc {4}{1}{Aufbau}{35}{0}{4}
\beamer@subsectionintoc {4}{2}{Perfomance}{36}{0}{4}
\beamer@subsectionintoc {4}{3}{Stabilit\IeC {\"a}t \& Sicherheit}{39}{0}{4}
\beamer@subsectionintoc {4}{4}{Ressourcenverbrauch}{41}{0}{4}
\beamer@sectionintoc {5}{Fazit}{43}{0}{5}
