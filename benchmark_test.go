package main

import "testing"

func BenchmarkFib10(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Fib(10)
	}
}

func BenchmarkFib25(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Fib(25)
	}
}

func BenchmarkFib50(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Fib(50)
	}
}

func BenchmarkConcat1000(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Concat(1000)
	}
}

func BenchmarkConcat10000(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Concat(10000)
	}
}

func BenchmarkConcat100000(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Concat(100000)
	}
}
