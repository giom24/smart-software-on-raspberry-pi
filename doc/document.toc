\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Einleitung}{3}% 
\contentsline {section}{\numberline {1.1}Smarte Projekte}{3}% 
\contentsline {section}{\numberline {1.2}Raspberry Pi}{3}% 
\contentsline {section}{\numberline {1.3}Smarte Projekte auf dem Raspberry PI}{4}% 
\contentsline {chapter}{\numberline {2}Unikernel}{5}% 
\contentsline {section}{\numberline {2.1}Was sind Unikernel}{5}% 
\contentsline {subsection}{\numberline {2.1.1}Ein-Adress-Maschine}{5}% 
\contentsline {subsection}{\numberline {2.1.2}Bibliotheks Betriebssystem}{6}% 
\contentsline {subsection}{\numberline {2.1.3}Arten von Unikernel}{7}% 
\contentsline {section}{\numberline {2.2}Vor- und Nachteile}{7}% 
\contentsline {subsection}{\numberline {2.2.1}Vorteile}{7}% 
\contentsline {subsection}{\numberline {2.2.2}Nachteile}{8}% 
\contentsline {section}{\numberline {2.3}Technologien im Vergleich}{8}% 
\contentsline {subsection}{\numberline {2.3.1}Virtuelle Maschinen}{8}% 
\contentsline {subsection}{\numberline {2.3.2}Linux Container}{9}% 
\contentsline {subsection}{\numberline {2.3.3}Unikernel}{10}% 
\contentsline {subsection}{\numberline {2.3.4}Vor- und Nachteile}{10}% 
\contentsline {section}{\numberline {2.4}Beispiele}{12}% 
\contentsline {chapter}{\numberline {3}Go Krazy}{13}% 
\contentsline {section}{\numberline {3.1}Was ist GoKrazy}{13}% 
\contentsline {section}{\numberline {3.2}Wie Funktioniert GoKrazy}{13}% 
\contentsline {subsection}{\numberline {3.2.1}Toolchain}{14}% 
\contentsline {subsection}{\numberline {3.2.2}Dateisysteme}{14}% 
\contentsline {subsection}{\numberline {3.2.3}Boot}{15}% 
\contentsline {subsection}{\numberline {3.2.4}Initialisierungssystem}{16}% 
\contentsline {subsection}{\numberline {3.2.5}Userland}{16}% 
\contentsline {subsection}{\numberline {3.2.6}Webinterface}{17}% 
\contentsline {subsection}{\numberline {3.2.7}Aktualisierungen \IeC {\"u}ber das Netzwerk}{17}% 
\contentsline {section}{\numberline {3.3}Kernel Konfiguration}{18}% 
\contentsline {section}{\numberline {3.4}Debugging}{18}% 
\contentsline {section}{\numberline {3.5}Benchmark}{19}% 
\contentsline {subsection}{\numberline {3.5.1}Aufbau}{19}% 
\contentsline {subsection}{\numberline {3.5.2}Performanz}{19}% 
\contentsline {subsection}{\numberline {3.5.3}Stabilit\IeC {\"a}t}{20}% 
\contentsline {subsection}{\numberline {3.5.4}Sicherheit}{20}% 
\contentsline {subsection}{\numberline {3.5.5}Ressourcen Verbrauch}{20}% 
\contentsline {section}{\numberline {3.6}Fazit}{21}% 
\contentsline {chapter}{\numberline {4}Quellen}{22}% 
